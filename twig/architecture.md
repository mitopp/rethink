# Twig / Architecture

## Deciding on a Directory Structure

### Example

```text
├── ...
├── templates/
│   ├── abstract/ ......................... Abstracts like Macros etc.
│   ├── component/ .....................(1) Templates for Twig Components
│   ├── email/ ............................ Templates for Email Rendering
│   ├── form/ ............................. Templates for Form Rendering
│   ├── page/ ............................. Templates for Page Rendering
│   └── base.html.twig .................... Base Template for Pages
├── ...
```

* (1): By default, `symfony/ux-twig-component` expects all classes in the `src/Twig/Components`
  directory. But the bundle can
  be [configured](https://symfony.com/bundles/ux-twig-component/current/index.html#configuration) accordingly. The
  default directory for the Twig templates can also be configured here.
