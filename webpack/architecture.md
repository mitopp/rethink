# Webpack Encore / Architecture

## Deciding on a Directory Structure

### Stylesheets

```text
├── ...
├── assets/ 
│   ├── controllers/ ...................... Stimulus Controllers
│   ├── scripts/ .......................... JavaScripts
│   ├── styles/ ........................... Stylesheets (SASS, Less, etc.)
│   │   ├── abstract/ ..................... Global items like mixins, variables etc.
│   │   │   ├── mixin/ .................... Global mixins
│   │   │   │   ├── _index.scss ........... Include childs with @forward
│   │   │   │   ├── bem.scss/ ............. Block, element and modifier
│   │   │   │   └── ...
│   │   │   ├── variable/ ................. Global variables
│   │   │   │   ├── _index.scss ........... Include childs with @forward
│   │   │   │   ├── color.scss ............ Color variables
│   │   │   │   ├── option.scss ........... Options
│   │   │   │   └── ...
│   │   │   └── _index.scss ............... Include childs with @forward
│   │   ├── base/ ......................... Base style rules like reset etc.
│   │   │   ├── _index.scss ............... Include childs with @forward
│   │   │   ├── print.scss ................ Print rules
│   │   │   ├── reset.scss ................ Reset rules
│   │   │   ├── type.scss ................. Typography rules
│   │   │   └── ...
│   │   ├── component/ .................... Styles for components
│   │   │   ├── _index.scss ............... Include childs with @forward
│   │   │   ├── footer.scss ............... Footer styles
│   │   │   ├── header.scss ............... Header styles
│   │   │   └── ...
│   │   ├── page/ ......................... Page specific styles
│   │   │   ├── _index.scss ............... Include childs with @forward
│   │   │   ├── login.scss ................ Custom styles for login page only
│   │   │   └── ...
│   │   ├── theme/ ........................ Theming
│   │   │   ├── _index.scss ............... Include childs with @forward
│   │   │   ├── default.scss .............. Custom CSS variables for theming
│   │   │   └── ...
│   │   ├── vendor/ ....................... Third-party bundles
│   │   │   ├── _index.scss ............... Include childs with @forward
│   │   │   ├── bootstrap.scss ............ Bootstrap Framework
│   │   │   ├── font-awesome.scss ......... Font Awesome Styles
│   │   │   └── ...
│   │   └── app.scss ...................... Combine all together with @use
│   ├── bootstrap.js ...................... Register all Stimulus Controllers
│   └── controllers.json .................. Describe all Stimulus Controllers
├── ...
```

```scss
/// file: app.scss

@use "abstract";
@use "vendor";
@use "base";
@use "component";
@use "page";
@use "theme";
```

```scss
/// file: vendor/boostrap.scss

// 1. Include functions first (so you can manipulate colors, SVGs, calc, etc)
@import "bootstrap/scss/functions";

// 2. Include any default variable overrides here
@import "../abstract/variable";

// 3. Include remainder of required Bootstrap stylesheets (including any separate color mode stylesheets)
@import "bootstrap/scss/variables";
@import "bootstrap/scss/variables-dark";

// 4. Include any default map overrides here

// 5. Include remainder of required parts
@import "bootstrap/scss/maps";
@import "bootstrap/scss/mixins";
@import "bootstrap/scss/root";

// 6. Optionally include any other parts as needed
@import "bootstrap/scss/root";
@import "bootstrap/scss/reboot";
@import "bootstrap/scss/type";
// ...
@import "bootstrap/scss/helpers";

// 7. Optionally include utilities API last to generate classes based on the Sass map in `_utilities.scss`
@import "bootstrap/scss/utilities";
@import "bootstrap/scss/utilities/api";

// 8. Add additional custom code here
```

### JavaScripts

#### Stimulus Controllers

***TBD***

#### Scripts

***TBD***
