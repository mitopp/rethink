# ReThink

* Architecture
    * [Symfony](symfony/architecture.md)
    * [Twig](twig/architecture.md)
    * [Webpack Encore](webpack/architecture.md)
