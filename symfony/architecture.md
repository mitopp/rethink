# Symfony / Architecture

## Deciding on a Directory Structure

### Generic

Minimal configured Symfony project.

```text
├── ...
├── bin/
├── config/
├── public/
├── src/
├── tests/
├── var/
├── vendor/
├── ...
```

### Nicer generic (Full example)

Since the Symfony framework already names many directories in singular, it makes sense to name all other directories in
singular as well. Each subfolder of `src` is created as a singular for consistency. As a result, some folders have to be
renamed manually and the configuration of the bundles has to be adjusted. More detailed information can be found under
the directory structure.

```text
├── ...
├── assets/ ............................... Assets (see Webpack/Architecture for more details)
├── bin/ .................................. Binaries like `console` etc.
├── config/ ............................... Configuration
├── migrations/ ........................... Migrations from Migration Bundle
├── public/
│   ├── build/ ............................ Compiled assets
│   ├── static/ ........................... Static assets
│   └── ...
├── src/
│   ├── Command/ .......................... Console Commands
│   ├── Contract/ ......................... Contracts (Interfaces)
│   ├── Controller/ ....................... Controllers
│   ├── Entity/ ........................... Entities
│   ├── Enum/ ............................. Enums
│   ├── Event/ ............................ Events
│   │   ├── Listener/ ..................... Event Listeners
│   │   └── Subscriber/ ................... Event Subscribers
│   ├── Exception/ ........................ Exceptions
│   ├── Fixture/ ...................... [1] Fixtures
│   ├── Form/ ............................. Forms
│   │   ├── Transformer/ .................. Data Transformers
│   │   └── Type/ ......................... Form Types
│   ├── Message/ .......................... Messages
│   │   └── Handler/ ...................... Message Handlers
│   ├── Model/ ............................ Models like DTOs etc.
│   ├── Notifier/ ......................... Notifications
│   ├── Repository/ ....................... Repositories
│   ├── Security/ ......................... Security classes like Authenticators, etc.
│   │   └── Voter/ ........................ Security Voters
│   ├── Serializer/ ....................... Serializers
│   ├── Twig/
│   │   ├── Component/ ................ [2] Twig Components
│   │   └── Extension/ .................... Twig Extensions (Filter, Functions, etc.)
│   ├── Validation/ ....................... Validators/Constraints
│   ├── ValueObject/ ...................... Value Objects
│   └── ...
├── templates/ ............................ Templates (see Twig/Architecture for more details)
├── tests/ ................................ Same structure like `src`
│   ├── Functional/  
│   ├── Integration/
│   ├── Unit/
│   └── ...
├── translations/ ......................... Translation strings
│   ├── messages+intl-icu.en.yaml ......... Domain: `messages`, Locale: `en`
│   ├── security+intl-icu.en.yaml ......... Domain: `security`, Locale: `en`
│   ├── validators+intl-icu.en.yaml ....... Domain: `validators`, Locale: `en`
│   └── ...
├── var/
│   ├── cache/
│   └── log/
└── vendor/
```

* **[1]**: `symfony/orm-fixtures` creates the `src/AppFixtures` directory by default. The bundle has (currently) no
  necessary configuration. Therefore renaming the directory from `AppFixtures` to `Fixtures` is sufficient.
* **[2]**: By default, `symfony/ux-twig-component` expects all classes in the `src/Twig/Components`
  directory. But the bundle can
  be [configured](https://symfony.com/bundles/ux-twig-component/current/index.html#configuration) accordingly. The
  default directory for the Twig templates can also be configured here.

More thoughts on [Twig/Architecture](../twig/architecture.md) and [Webpack/Architecture](../webpack/architecture.md).
